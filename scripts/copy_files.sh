#!/usr/bin/env bash

echo "copying extra files"

MNTDIR="mnt"

cp templates/ifcfg-interface-eth0 $MNTDIR/etc/network/interfaces.d/eth0

mkdir $MNTDIR/home/pi/Documents
cp -v b2documents/teamb2.py $MNTDIR/home/pi/Documents/teamb2.py
